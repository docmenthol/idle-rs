use sdl2::pixels::Color;

#[non_exhaustive]
pub struct UIColors;

impl UIColors {
    pub const NEUTRAL: Color = Color::RGB(50, 50, 50);
    pub const NEUTRAL_HIGHLIGHT: Color = Color::RGB(75, 75, 75);
    pub const NEUTRAL_ACTIVE: Color = Color::RGB(100, 100, 100);

    pub const PRIMARY: Color = Color::RGB(41, 74, 122);
    pub const PRIMARY_HIGHLIGHT: Color = Color::RGB(66, 150, 250);
    pub const PRIMARY_ACTIVE: Color = Color::RGB(15, 135, 250);

    pub const SECONDARY: Color = Color::RGB(117, 41, 122);
    pub const SECONDARY_HIGHLIGHT: Color = Color::RGB(138, 64, 250);
    pub const SECONDARY_ACTIVE: Color = Color::RGB(135, 42, 250);

    pub const BACKGROUND: Color = Color::RGB(0, 0, 0);
    pub const TEXT: Color = Color::RGB(255, 255, 255);
}
