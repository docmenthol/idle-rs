use sdl2::{
    // mixer::{InitFlag, Sdl2MixerContext, DEFAULT_FORMAT},
    ttf::Sdl2TtfContext,
    video::Window,
    Sdl,
};

pub fn init(width: u32, height: u32) -> Result<(Sdl, Window, Sdl2TtfContext), String> {
    let sdl = sdl2::init()?;
    let video = sdl.video()?;
    let ttf = match sdl2::ttf::init() {
        Ok(ttf) => ttf,
        Err(err) => panic!("Could not initialize sdl2_ttf. Error: {}", err),
    };

    // sdl.audio()?;
    // sdl2::mixer::open_audio(44_100, DEFAULT_FORMAT, 1, 1_024)?;

    // let mixer = sdl2::mixer::init(InitFlag::from_bits_truncate(0))?;

    // sdl2::mixer::allocate_channels(4);

    let window = video
        .window("idle-rs", width, height)
        .position_centered()
        .opengl()
        .build()
        .map_err(|e| e.to_string())?;

    Ok((sdl, window, ttf))
}
