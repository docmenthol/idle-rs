use sdl2::render::Texture;
use sdl2::render::TextureCreator;
use sdl2::ttf::Font;
use sdl2::video::WindowContext;
use sdl2::{rect::Rect, render::Canvas, video::Window};

use crate::{colors::UIColors, ui::ColorTheme};

pub struct ProgressBar<'a> {
    rect: Rect,
    pub max: f32,
    pub current: f32,
    fill_rate: f32,
    color_theme: ColorTheme,
    texture_creator: &'a TextureCreator<WindowContext>,
    font: &'a Font<'a, 'a>,
    texture: Texture<'a>,
    label_position: Rect,
}

fn float_min(a: f32, b: f32) -> f32 {
    if a < b {
        a
    } else {
        b
    }
}

fn float_max(a: f32, b: f32) -> f32 {
    if a > b {
        a
    } else {
        b
    }
}

impl ProgressBar<'_> {
    pub fn new<'a>(
        rect: Rect,
        label: &'static str,
        max: f32,
        fill_rate: f32,
        color_theme: ColorTheme,
        font: &'a Font<'a, 'a>,
        texture_creator: &'a TextureCreator<WindowContext>,
    ) -> Result<ProgressBar<'a>, String> {
        let label_surface = font
            .render(label)
            .solid(UIColors::TEXT)
            .map_err(|e| e.to_string())?;
        let label_texture = texture_creator
            .create_texture_from_surface(&label_surface)
            .map_err(|e| e.to_string())?;

        Ok(ProgressBar {
            rect,
            max,
            current: 0.0,
            fill_rate,
            color_theme,
            texture_creator,
            font,
            texture: label_texture,
            label_position: Rect::new(
                rect.x + 10,
                rect.y + 10,
                label_surface.width(),
                label_surface.height(),
            ),
        })
    }

    pub fn set_level(&mut self, current: f32) {
        self.current = float_min(current, self.max);
    }

    pub fn increase_level_by(&mut self, amount: f32) {
        self.current = float_min(self.current + amount, self.max);
    }

    // pub fn set_fill_rate(&mut self, fill_rate: f32) {
    //     self.fill_rate = fill_rate;
    // }

    pub fn tick(&mut self, dtf: f32) {
        let divisor = if self.fill_rate <= 0.0 {
            1.0
        } else {
            self.fill_rate
        };
        self.increase_level_by(dtf / divisor);
    }

    pub fn full(&self) -> bool {
        self.current == self.max
    }

    pub fn render(&self, renderer: &mut Canvas<Window>) -> Result<(), String> {
        renderer.set_draw_color(self.color_theme.to_base_color());
        renderer.draw_rect(self.rect)?;

        // Don't try to animate stuff that's too fast.
        let progress_width = if self.fill_rate <= 1.0 {
            self.rect.width() as f32 - 10.0
        } else {
            float_max(
                (self.current as f32 / self.max as f32) * self.rect.width() as f32 - 10.0,
                0.0,
            )
        };

        // Skip rendering the progress if it's 0. Prevents rendering a
        // single line for an extra tick.
        if progress_width > 0.0 {
            let progress_rect = Rect::new(
                self.rect.x + 5,
                self.rect.y + 5,
                progress_width as u32,
                self.rect.height() - 10,
            );

            renderer.fill_rect(progress_rect)?;
        }

        renderer.copy(&self.texture, None, self.label_position)?;

        // When the fill rate is low enough, this should display
        // the things/second rate instead.
        let progress_string = if self.fill_rate <= 1.0 {
            let rate = self.fill_rate * 1000.0;
            format!("{:.0} / second", rate)
        } else {
            format!("{:.0} / {:.0}", self.current, self.max)
        };

        let progress_text_surface = self
            .font
            .render(progress_string.as_str())
            .solid(UIColors::TEXT)
            .map_err(|e| e.to_string())?;
        let progress_text_texture = self
            .texture_creator
            .create_texture_from_surface(&progress_text_surface)
            .map_err(|e| e.to_string())?;
        let progress_text_position = Rect::new(
            self.rect.x + 10,
            self.rect.y + 25,
            progress_text_surface.width(),
            progress_text_surface.height(),
        );

        renderer.copy(&progress_text_texture, None, progress_text_position)?;

        Ok(())
    }
}
