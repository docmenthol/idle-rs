use sdl2::{
    mouse::MouseState,
    rect::Rect,
    render::{Canvas, Texture, TextureCreator},
    ttf::Font,
    video::{Window, WindowContext},
};

use crate::{colors::UIColors, ui::ColorTheme};

#[derive(Clone, Copy)]
pub enum ButtonStyle {
    Filled,
    Outlined,
}

pub struct Button<'a> {
    pub rect: Rect,
    color_theme: ColorTheme,
    bstyle: ButtonStyle,
    pub hovered: bool,
    pub clicked: bool,
    texture_creator: &'a TextureCreator<WindowContext>,
    font: &'a Font<'a, 'a>,
    texture: Texture<'a>,
    label_position: Rect,
}

impl Button<'_> {
    pub fn new<'a>(
        rect: Rect,
        label: &'static str,
        color_theme: ColorTheme,
        bstyle: ButtonStyle,
        font: &'a Font<'a, 'a>,
        texture_creator: &'a TextureCreator<WindowContext>,
    ) -> Result<Button<'a>, String> {
        let label_surface = font
            .render(label)
            .solid(UIColors::TEXT)
            .map_err(|e| e.to_string())?;
        let label_texture = texture_creator
            .create_texture_from_surface(&label_surface)
            .map_err(|e| e.to_string())?;
        let label_x = rect.x as u32 + (rect.width() / 2) - (label_surface.width() / 2);
        let label_y = rect.y as u32 + (rect.height() / 2) - (label_surface.height() / 2);

        Ok(Button {
            rect,
            color_theme,
            bstyle,
            clicked: false,
            hovered: false,
            texture_creator,
            font,
            texture: label_texture,
            label_position: Rect::new(
                label_x as i32,
                label_y as i32,
                label_surface.width(),
                label_surface.height(),
            ),
        })
    }

    pub fn handle_event(&mut self, mouse: &MouseState) {
        let (x, y) = (mouse.x(), mouse.y());
        let mut inside = true;

        if (x < self.rect.x)
            || (x > self.rect.x + (self.rect.width() as i32))
            || (y < self.rect.y)
            || (y > self.rect.y + (self.rect.height() as i32))
        {
            inside = false;
        }

        if !inside {
            self.hovered = false;
            self.clicked = false;
        } else {
            self.hovered = true;
            if mouse.left() {
                self.clicked = true;
            } else {
                self.clicked = false;
            }
        }
    }

    pub fn update_label<'a>(&mut self, label: &'static str) -> Result<(), String> {
        let label_surface = self
            .font
            .render(label)
            .solid(UIColors::TEXT)
            .map_err(|e| e.to_string())?;
        let label_texture = self
            .texture_creator
            .create_texture_from_surface(&label_surface)
            .map_err(|e| e.to_string())?;
        let label_x = self.rect.x as u32 + (self.rect.width() / 2) - (label_surface.width() / 2);
        let label_y = self.rect.y as u32 + (self.rect.height() / 2) - (label_surface.height() / 2);

        self.texture = label_texture;
        self.label_position = Rect::new(
            label_x as i32,
            label_y as i32,
            label_surface.width(),
            label_surface.height(),
        );

        Ok(())
    }

    pub fn render(&self, renderer: &mut Canvas<Window>) -> Result<(), String> {
        match self.bstyle {
            ButtonStyle::Filled => self.render_filled(renderer),
            ButtonStyle::Outlined => self.render_stroked(renderer),
        }
    }

    fn render_filled(&self, renderer: &mut Canvas<Window>) -> Result<(), String> {
        if self.clicked {
            renderer.set_draw_color(self.color_theme.to_active_color());
        } else if !self.clicked && self.hovered {
            renderer.set_draw_color(self.color_theme.to_highlight_color());
        } else {
            renderer.set_draw_color(self.color_theme.to_base_color());
        }

        renderer.fill_rect(self.rect)?;

        renderer.copy(&self.texture, None, self.label_position)?;

        Ok(())
    }

    fn render_stroked(&self, renderer: &mut Canvas<Window>) -> Result<(), String> {
        if self.clicked {
            renderer.set_draw_color(self.color_theme.to_active_color());
            renderer.fill_rect(self.rect)?;
        } else if !self.clicked && self.hovered {
            renderer.set_draw_color(self.color_theme.to_highlight_color());
            renderer.fill_rect(self.rect)?;
        } else {
            renderer.set_draw_color(self.color_theme.to_base_color());
            renderer.draw_rect(self.rect)?;
        }

        renderer.copy(&self.texture, None, self.label_position)?;

        Ok(())
    }
}
