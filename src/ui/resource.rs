#![allow(dead_code)]
use crate::strings::UIStrings;
use crate::ui::button::Button;
use crate::ui::progress_bar::ProgressBar;
use crate::ButtonStyle;
use crate::ColorTheme;
use sdl2::mouse::MouseState;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::render::TextureCreator;
use sdl2::ttf::Font;
use sdl2::video::Window;
use sdl2::video::WindowContext;

#[derive(Clone, Copy)]
pub struct ProgressConfig {
    label: &'static str,
    max: f32,
    fill_rate: f32,
    rect: Rect,
}

#[derive(Clone, Copy)]
pub struct ButtonConfig {
    label: &'static str,
    button_style: ButtonStyle,
    rect: Rect,
}

#[derive(Clone, Copy)]
pub struct ResourceBuilder {
    color_theme: ColorTheme,
    progress_bar_config: Option<ProgressConfig>,
    button_config: Option<ButtonConfig>,
}

pub struct Resource<'a> {
    font: &'a Font<'a, 'a>,
    texture_creator: &'a TextureCreator<WindowContext>,
    pub running: bool,
    pub progress_bar: ProgressBar<'a>,
    pub button: Button<'a>,
}

impl ResourceBuilder {
    pub fn new(color_theme: ColorTheme) -> ResourceBuilder {
        ResourceBuilder {
            color_theme,
            progress_bar_config: None,
            button_config: None,
        }
    }

    pub fn progress(
        &mut self,
        label: &'static str,
        max: f32,
        fill_rate: f32,
        rect: Rect,
    ) -> &mut ResourceBuilder {
        self.progress_bar_config = Some(ProgressConfig {
            label,
            max,
            fill_rate,
            rect,
        });
        self
    }

    pub fn button(
        &mut self,
        label: &'static str,
        button_style: ButtonStyle,
        rect: Rect,
    ) -> &mut ResourceBuilder {
        self.button_config = Some(ButtonConfig {
            label,
            button_style,
            rect,
        });
        self
    }

    pub fn build<'a>(
        self,
        font: &'a Font<'a, 'a>,
        texture_creator: &'a TextureCreator<WindowContext>,
    ) -> Result<Resource<'a>, String> {
        match (self.progress_bar_config, self.button_config) {
            (Some(pbc), Some(bc)) => {
                Resource::new(self.color_theme, pbc, bc, font, texture_creator)
            }
            (_, _) => Err("resource ui elements not configured".to_string()),
        }
    }
}

impl Resource<'_> {
    pub fn new<'a>(
        color_theme: ColorTheme,
        progress_bar_config: ProgressConfig,
        button_config: ButtonConfig,
        font: &'a Font<'a, 'a>,
        texture_creator: &'a TextureCreator<WindowContext>,
    ) -> Result<Resource<'a>, String> {
        let progress_bar = ProgressBar::new(
            progress_bar_config.rect,
            progress_bar_config.label,
            progress_bar_config.max,
            progress_bar_config.fill_rate,
            color_theme,
            &font,
            &texture_creator,
        )?;
        let button = Button::new(
            button_config.rect,
            button_config.label,
            color_theme,
            button_config.button_style,
            &font,
            &texture_creator,
        )?;

        Ok(Resource {
            font,
            texture_creator,
            progress_bar,
            button,
            running: false,
        })
    }

    pub fn update(
        &mut self,
        deltatime: f32,
        mouse: &MouseState,
        mouse_lock: bool,
    ) -> Result<(), String> {
        if self.progress_bar.full() {
            self.progress_bar.set_level(0.0);
        }

        if self.running {
            self.progress_bar.tick(deltatime);
        }

        self.button.handle_event(&mouse);

        if self.button.clicked && !mouse_lock {
            self.running = !self.running;

            if self.running {
                self.button.update_label(UIStrings::PAUSE)?;
            } else {
                self.button.update_label(UIStrings::RUN)?;
            }
        }

        Ok(())
    }

    pub fn render(&mut self, renderer: &mut Canvas<Window>) -> Result<(), String> {
        self.progress_bar.render(renderer)?;
        self.button.render(renderer)?;

        Ok(())
    }
}
