#![allow(dead_code)]

use sdl2::rect::Rect;

#[derive(Clone, Copy)]
pub enum ColumnCount {
    OneColumn = 1,
    TwoColumns = 2,
    ThreeColumns = 3,
    FourColumns = 4,
    SixColumns = 6,
    TwelveColumns = 12,
}

#[derive(PartialEq, PartialOrd, Clone, Copy)]
pub enum ColumnNumber {
    One = 1,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Eleven,
    Twelve,
}

#[derive(Clone, Copy)]
pub struct Grid {
    columns: ColumnCount,
    column_width: u32,
    row_height: u32,
    gutter_width: u32,
    gutter_height: u32,
    offset_x: i32,
    offset_y: i32,
}

impl Grid {
    pub fn new(
        columns: ColumnCount,
        column_width: u32,
        row_height: u32,
        gutter_width: u32,
        gutter_height: u32,
        offset_x: i32,
        offset_y: i32,
    ) -> Self {
        Grid {
            columns,
            column_width,
            row_height,
            gutter_width,
            gutter_height,
            offset_x,
            offset_y,
        }
    }

    pub fn width(self) -> u32 {
        self.width_for(self.columns)
    }

    pub fn width_for(self, columns: ColumnCount) -> u32 {
        let cs = columns as u32;
        (cs * self.column_width) + (self.gutter_width * cs) + self.gutter_width
    }

    pub fn height_for(self, rows: u32) -> u32 {
        (self.gutter_height * rows) + (self.row_height * rows) + self.gutter_height
    }

    pub fn columns(
        self,
        row: u32,
        start_column: ColumnNumber,
        columns: ColumnCount,
    ) -> Result<Rect, String> {
        let sc = start_column as u32;
        let cols = columns as u32;

        let y = (row * self.row_height) + (row * self.gutter_height) - self.row_height;
        let x = (sc * self.column_width) + (self.gutter_width * sc) - self.column_width;
        let width = (cols * self.column_width) + (cols * self.gutter_width) - self.gutter_width;

        Ok(Rect::new(
            x as i32 + self.offset_x,
            y as i32 + self.offset_y,
            width,
            self.row_height,
        ))
    }

    pub fn single_column(self, row: u32, column: ColumnNumber) -> Result<Rect, String> {
        let c = column as u32;

        let y = (row * self.row_height) + (row * self.gutter_height) - self.row_height;
        let x = ((c - 1) * self.column_width) + (c * self.gutter_width);
        let width = self.column_width;

        Ok(Rect::new(
            x as i32 + self.offset_x,
            y as i32 + self.offset_y,
            width,
            self.row_height,
        ))
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        ColumnCount::{FourColumns, OneColumn, ThreeColumns},
        ColumnNumber::One,
        Grid,
    };
    use sdl2::rect::Rect;

    #[test]
    fn calculates_single_column_width() {
        let grid = Grid::new(OneColumn, 16, 16, 2, 2, 0, 0);
        assert_eq!(grid.width(), 2 + 16 + 2);
    }

    #[test]
    fn calculates_single_column_rect() {
        let grid = Grid::new(OneColumn, 16, 16, 2, 2, 0, 0);
        let expected = Rect::new(2, 2, 16, 16);
        match grid.single_column(1, One) {
            Ok(actual) => assert_eq!(actual, expected),
            Err(_) => assert!(false),
        }
    }

    #[test]
    fn calculates_three_fourths_column_width() {
        let grid = Grid::new(FourColumns, 16, 16, 2, 2, 0, 0);
        assert_eq!(grid.width_for(ThreeColumns), 2 + 16 + 2 + 16 + 2 + 16 + 2);
    }

    #[test]
    fn calculates_three_fourths_column_rect() {
        let grid = Grid::new(FourColumns, 16, 16, 2, 2, 0, 0);
        let expected = Rect::new(2, 2, 16 * 3 + 4, 16);
        match grid.columns(1, One, ThreeColumns) {
            Ok(actual) => assert_eq!(actual, expected),
            Err(_) => assert!(false),
        }
    }
}
