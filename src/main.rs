use sdl2::event::Event;
use std::{path::Path, time::Instant};

mod colors;
mod strings;
mod substrate;
mod ui;

use crate::{
    colors::UIColors,
    strings::UIStrings,
    ui::{
        button::ButtonStyle,
        grid::{ColumnCount, ColumnNumber, Grid},
        resource::{Resource, ResourceBuilder},
        ColorTheme,
    },
};

fn main() -> Result<(), String> {
    let grid = Grid::new(ColumnCount::FourColumns, 64, 64, 10, 10, 0, 0);

    let (sdl_context, window, ttf_context) =
        crate::substrate::init(grid.width(), grid.height_for(6))?;

    let mut renderer = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .map_err(|e| e.to_string())?;

    let texture_creator = renderer.texture_creator();

    let font = match ttf_context.load_font(&Path::new("resources/ProggyClean.ttf"), 15) {
        Ok(font) => font,
        Err(err) => panic!("Could not load font. Error: {}", err),
    };

    let mut now = Instant::now();
    let mut dt: u32;

    let mut app_running = true;
    let mut mouse_lock = false;
    let mut event_pump = sdl_context.event_pump()?;

    let mut resources: [Resource; 4] = [
        ResourceBuilder::new(ColorTheme::Nothing)
            .progress(
                UIStrings::QUARKS,
                100.0,
                250.0,
                grid.columns(1, ColumnNumber::One, ColumnCount::ThreeColumns)?,
            )
            .button(
                UIStrings::RUN,
                ButtonStyle::Outlined,
                grid.single_column(1, ColumnNumber::Four)?,
            )
            .build(&font, &texture_creator)?,
        ResourceBuilder::new(ColorTheme::Primary)
            .progress(
                UIStrings::PROTONS,
                100.0,
                500.0,
                grid.columns(2, ColumnNumber::One, ColumnCount::ThreeColumns)?,
            )
            .button(
                UIStrings::RUN,
                ButtonStyle::Filled,
                grid.single_column(2, ColumnNumber::Four)?,
            )
            .build(&font, &texture_creator)?,
        ResourceBuilder::new(ColorTheme::Secondary)
            .progress(
                UIStrings::ELECTRONS,
                100.0,
                1000.0,
                grid.columns(3, ColumnNumber::One, ColumnCount::ThreeColumns)?,
            )
            .button(
                UIStrings::RUN,
                ButtonStyle::Outlined,
                grid.single_column(3, ColumnNumber::Four)?,
            )
            .build(&font, &texture_creator)?,
        ResourceBuilder::new(ColorTheme::Nothing)
            .progress(
                UIStrings::NEUTRONS,
                100.0,
                2000.0,
                grid.columns(4, ColumnNumber::One, ColumnCount::ThreeColumns)?,
            )
            .button(
                UIStrings::RUN,
                ButtonStyle::Filled,
                grid.single_column(4, ColumnNumber::Four)?,
            )
            .build(&font, &texture_creator)?,
    ];

    resources[0].progress_bar.set_level(15.0);
    resources[1].progress_bar.set_level(32.0);
    resources[2].progress_bar.set_level(57.0);
    resources[3].progress_bar.set_level(83.0);

    while app_running {
        dt = now.elapsed().subsec_millis();
        now = Instant::now();

        let dtf = dt as f32;
        let mouse = event_pump.mouse_state();

        if !mouse.left() {
            mouse_lock = false;
        }

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => app_running = false,
                _ => {}
            }
        }

        resources[0].update(dtf, &mouse, mouse_lock)?;
        resources[1].update(dtf, &mouse, mouse_lock)?;
        resources[2].update(dtf, &mouse, mouse_lock)?;
        resources[3].update(dtf, &mouse, mouse_lock)?;

        if resources[0].button.clicked
            || resources[1].button.clicked
            || resources[2].button.clicked
            || resources[3].button.clicked
        {
            mouse_lock = true;
        }

        renderer.set_draw_color(UIColors::BACKGROUND);
        renderer.clear();

        resources[0].render(&mut renderer)?;
        resources[1].render(&mut renderer)?;
        resources[2].render(&mut renderer)?;
        resources[3].render(&mut renderer)?;

        renderer.present();
    }

    Ok(())
}
