#[non_exhaustive]
pub struct UIStrings;

impl UIStrings {
    pub const RUN: &'static str = "run";
    pub const PAUSE: &'static str = "pause";

    pub const QUARKS: &'static str = "quarks";
    pub const PROTONS: &'static str = "protons";
    pub const ELECTRONS: &'static str = "electrons";
    pub const NEUTRONS: &'static str = "neutrons";
}
