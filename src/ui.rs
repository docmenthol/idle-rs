use sdl2::pixels::Color;

use crate::colors::UIColors;

pub mod button;
pub mod grid;
pub mod progress_bar;
pub mod resource;

#[derive(Clone, Copy)]
pub enum ColorTheme {
    Nothing,
    Primary,
    Secondary,
}

impl ColorTheme {
    pub fn to_base_color(&self) -> Color {
        match self {
            ColorTheme::Nothing => UIColors::NEUTRAL,
            ColorTheme::Primary => UIColors::PRIMARY,
            ColorTheme::Secondary => UIColors::SECONDARY,
        }
    }

    pub fn to_highlight_color(&self) -> Color {
        match self {
            ColorTheme::Nothing => UIColors::NEUTRAL_HIGHLIGHT,
            ColorTheme::Primary => UIColors::PRIMARY_HIGHLIGHT,
            ColorTheme::Secondary => UIColors::SECONDARY_HIGHLIGHT,
        }
    }

    pub fn to_active_color(&self) -> Color {
        match self {
            ColorTheme::Nothing => UIColors::NEUTRAL_ACTIVE,
            ColorTheme::Primary => UIColors::PRIMARY_ACTIVE,
            ColorTheme::Secondary => UIColors::SECONDARY_ACTIVE,
        }
    }
}
