# idle-rs

[![stable](https://gitlab.com/docmenthol/idle-rs/badges/dev/pipeline.svg?job=rust-latest)](https://gitlab.com/docmenthol/idle-rs/-/jobs)
[![nightly](https://gitlab.com/docmenthol/idle-rs/badges/dev/pipeline.svg?job=rust-nightly)](https://gitlab.com/docmenthol/idle-rs/-/jobs)

A basic incremental game written in Rust. This is a stepping stone for learning both Rust and SDL, as well as some general techniques for using them together. Later this will all be applied to a more complex game.

### License

MIT
